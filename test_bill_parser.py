import line_parser
import main
import pytest

def test_correct_lines():

    line = "00:01:07,400-234-090"
    output = line_parser.parse(line)
    assert output == (400234090, 67)

    line = "00:05:01,701-080-080"
    output = line_parser.parse(line)
    assert output == (701080080, 301)

    line = "00:05:00,400-234-090"
    output = line_parser.parse(line)
    assert output == (400234090, 300)

def test_incorrect_lines():
    line = "00:01:07,400-234-090-500"
    output = line_parser.parse(line)
    assert output == None

    line = "00:01:07,400-234-090 400-234-090"
    output = line_parser.parse(line)
    assert output == None

    line = "00:05:01,001-080-080"
    output = line_parser.parse(line)
    assert output == None

    line = "00:05:00 400-234-090"
    output = line_parser.parse(line)
    assert output == None

    line = "005300 400-234-090"
    output = line_parser.parse(line)
    assert output == None


def test_parse_bill():
    bill_lines = """00:01:07,400-234-090
00:05:01,701-080-080
00:05:00,400-234-090"""

    total_bill = main.phone_charges(bill_lines)
    assert total_bill == 900

    bill_lines = """00:01:00,400-234-090
00:01:01,701-080-080
00:05:00,400-234-090
00:15:00,000-234-090
00:05:01,701-080-080
00:10:00,400-234-090
"""
    total_bill = main.phone_charges(bill_lines)
    assert total_bill == 900 + 183

    bill_lines = ""
    with pytest.raises(Exception):
        main.phone_charges(bill_lines)

