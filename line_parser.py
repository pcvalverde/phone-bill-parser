import time
import re
from collections import namedtuple

class FormatError(Exception):
    """
    Custom exception for format errors in bill lines
    """
    pass


def _get_seconds(call_len: str) -> int:
    """
    Gets the number of seconds of a time
    Args:
        call_len: a time string with format "hh-mm-ss"

    Returns: number of seconds
    """
    call_time = time.strptime(call_len, "%H:%M:%S")
    return call_time.tm_hour * 3600 + call_time.tm_min * 60 + call_time.tm_sec


def _get_phone(number: str) -> int:
    """
    Gets a phone number as int if the format is correct
    Args:
        number: a string number with format nnn-nnn-nnn with no leading zeroes

    Returns: an int phone number or Exception if format is not correct

    """
    pattern = r'^\d{3}-\d{3}-\d{3}$'
    if re.compile(pattern).match(number):
        phone_number = ''.join(number.split('-'))
        if phone_number[0] == '0':
            raise FormatError(f"Wrong phone number ´{number}- leading zeros \n")
        return int(phone_number)
    else:
        raise FormatError(f"Wrong phone number {number} - does not match nnn-nnn-nnn \n")


def parse(bill_line: str) -> (int, int):
    """
    Parses a line of the bill to get the phone number and the call seconds
    Args:
        bill_line: a line with a phone number and a call duration

    Returns (namedTuple): a tuple with the int phone number and the call seconds

    """
    CallTime = namedtuple("CallTime", ['phone', 'secs'])
    try:
        items = bill_line.split(',', 1)
        if len(items) != 2:
            raise FormatError("Wrong bill line {bill_line}. Expected 2 items separated with comma\n")
        return CallTime(_get_phone(items[1]), _get_seconds(items[0]))
    except Exception as e:
        print(e)
        return




