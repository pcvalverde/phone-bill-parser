import line_parser as line
import argparse
import sys

def _calculate_cost(seconds: int) -> int:
    """
    Calculates the cost of a call. For calls below 5 min, 3 cents/s.
    For the rest 150 cents/min round up
    Args:
        seconds: call seconds

    Returns: cost of the call in cents

    """
    if seconds < 300:
        return seconds * 3
    else:
        return (int(seconds/60) + (seconds % 60 > 0)) * 150


def phone_charges(calls: str) -> int:
    """
    Calculate the phone charges of a list of calls.
    The phone number with the largest number of seconds is not charged.
    Lines not following the expected format are omitted.
    Empty calls or a number of calls bigger than 100 are not allowed.

    Args:
        calls: a multiline string with phone numbers and call duration separated with commas

    Returns: total cost of the bill in cents

    """
    bill = {}
    free_phone = None
    bill_calls = [line.parse(call) for call in calls.splitlines()]
    if len(bill_calls) > 100 or len(bill_calls) <= 0:
        raise Exception(f"Expected a bill of 1 or 100 calls, but was {len(bill_calls)}\n")
    for call in bill_calls:
        if call is None: continue
        cost = _calculate_cost(call.secs)
        if call.phone in bill:
            bill[call.phone]['cost'] += cost
            bill[call.phone]['time'] += call.secs
        else:
            bill[call.phone] = dict([('time', call.secs), ('cost', cost)])
        if (
            free_phone is None or
            bill[call.phone]['time'] > bill[free_phone]['time'] or
            (bill[call.phone]['time'] == bill[free_phone]['time'] and call.phone < free_phone)
        ):
            free_phone = call.phone
    return sum(call['cost'] for phone, call in bill.items() if phone != free_phone)


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description="Calculate the total cost of a bill in e")
    arg_parser.add_argument("bill_calls", type=str, nargs=1,
                            help=r"A multiline string using \n with the format hh:mm:ss,nnn-nnn-nnn "
                                 r"For instance: 00:01:03,400-234-090\n00:01:01,701-080-080")
    args = arg_parser.parse_args()
    #Powershell adds a backslash to \n
    bill_calls = args.bill_calls[0].replace('\\n', '\n')
    total_cost = phone_charges(bill_calls)/100
    print(f"Bill total cost {total_cost:.2f} €")

